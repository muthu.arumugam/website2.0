import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Header extends Component {
  constructor(props) {
    super(props)
  this.state={
    isToggle:false
  }
    this.toggle = this.toggle.bind(this);

  }
  toggle() {
    console.log("This")
    this.setState(prevState => ({
      isToggle: !prevState.isToggle
    }));

  }
  render() {
    return (
      <div className="NavContainer">
        <div className="topnav">
          <ul className="social">
            <li><a href="#"><img src="twitter.png" /></a></li>
            <li><a href="#"><img src="linkedin.png" /></a></li>
            <li><a href="#"><img src="facebook.png" /></a></li>
            <li><a href="#"><img src="mail.png" /></a></li>
            <li><a href="#"><img src="call.png" />
              <span className="mobile">
                <span>8888888888</span>
                <span>Give us a call</span>
              </span>
            </a></li>
          </ul>
        </div>
        <nav className="navbar navbar-expand-lg p-0">
          <div className="container-fluid p-0">
            <Link to="/" className="nav-link navbar-brand"><img src="logo.png" className="log" /></Link>
            <button className="navbar-toggler" type="button" onClick={this.toggle} data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"><img src ="favicon.png"></img></span>
            </button>
            <div className={'collapse navbar-collapse ' + (this.state.isToggle ? 'show' : ' ')} id="navbarNav">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to="/aboutus" className="nav-link">Aboutus</Link>
                </li>
                <li className="nav-item">
                  <Link to="/course" className="nav-link">Course</Link>
                </li>
                <li className="nav-item">
                  <Link to="/xr-learning" className="nav-link">xr learning</Link>
                </li>
                <li className="nav-item">
                  <Link to="/events" className="nav-link">Events</Link>
                </li>
                <li className="nav-item">
                  <Link to="/contacts" className="nav-link">Contacts</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

