import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'
import Marquee from "react-fast-marquee";
export default class Homepage extends Component {
    render() {
        return (
            <div className="homepage">
                <div className="landing-sections">
                    <div className="content-section">
                        <div className="NavContainer">
                            <Header />
                            <div className="contents">
                                <h1><span>Advance</span> your career </h1>
                                <a href="#" class="custom-btn pa black">Learn More</a>
                            </div>
                            <div className="class-room">
                                <h2>A Modern Classroom</h2>
                                <p>Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!</p>
                                <div className="btn-container">
                                    <a href="#" class="custom-btn black">online class</a>
                                    <a href="#" class="custom-btn black">offline class</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="sections">
                    <div className="short-content">
                        <h2 className="mb-75">Why Data Science?</h2>
                        <p>Companies have forged ahead from trial-and-error to data-driven decisions. Data analysis is succoring organizations to unlock a whole new level of relevance in every move they make. It helps them spot the right opportunities, market to the right people at the right time, recruit the right talent and what not? It's no surprise that companies are dumping millions into this space.</p>
                    </div>
                    <div className="grid-section content-section mt-127">
                        <div className="image-container">
                            <img src="draw1.png" />
                        </div>
                        <div className="grid-content-container">
                            <div className="custom-col mr-40">
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
                </span>
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
                </span>
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
          </span>
                            </div>
                            <div className="custom-col">
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
          </span>
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
          </span>
                                <span>
                                    Offline and online training programs have their own pros and cons. But we help you get the best of both worlds. Pick the one that works for you!
          </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sections">
                    <div className="short-content">
                        <h2 className="mb-75">Collaborative Institute Map</h2>
                        <div className="image-container1 text-center">
                            <img src="map.png" class="img-fluid" />
                        </div>
                    </div>
                </div>
                <div className="sections">
                    <h2 className="mb-75">Variety of courses</h2>
                    <div className="courses content-section">
                        <div className="course">
                            <img src="c1.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                        <div className="course">
                            <img src="c2.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                        <div className="course">
                            <img src="c3.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                    </div>
                    <div className="content-section h-100">
                        <a href="#" className="custom-btn black d-block">Brouse Courses</a>
                    </div>
                </div>
                <br />
                <div className="sections">
                    <h2 className="mb-75">Testimonials</h2>
                    <div className="testimonials">
                        <Marquee>
                        <div className="testimonial">
                            <img src="user.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                        <div className="testimonial">
                            <img src="user.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                        <div className="testimonial">
                            <img src="user.png" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eni</p>
                        </div>
                        </Marquee>
                    </div>
                </div>
                <div className="sections">
                    <h2 className="mb-75">Our Partners</h2>
                    <div className="partners">
                        <div className="partner">
                            <img src="p1.png" />
                        </div>
                        <div className="partner">
                            <img src="p2.png" />
                        </div>
                        <div className="partner">
                            <img src="p3.png" />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
